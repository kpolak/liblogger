#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/support/date_time.hpp>



namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace expr = boost::log::expressions;
namespace keywords = boost::log::keywords;


#include <cstdio>
#include <cstdarg>


using namespace logging::trivial;
src::severity_logger_mt< severity_level > lg;

namespace{
    bool initializied = false;
}

void initLog_()
{
    if(initializied)
        return;

    logging::add_file_log
            (
                keywords::file_name = "logs/runtime_%N.log",                                        /*< file name pattern >*/
                keywords::open_mode = std::ios_base::app ,
                keywords::rotation_size = 10 * 1024 * 1024,
                keywords::auto_flush = true,
                keywords::time_based_rotation = sinks::file::rotation_at_time_point(0, 0, 0), /*< ...or at midnight >*/
                keywords::format =(
                expr::stream
                    << "["
                    << expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%H:%M:%S")
                    << "]:[" << logging::trivial::severity
                    << "] " << expr::smessage
            )
            );

    logging::core::get()->set_filter
            (
                logging::trivial::severity >= logging::trivial::debug
                );
     logging::add_common_attributes();
     initializied = true;

}

void LogDebug_(const char *format,...)
{
    char buffer[256];
    va_list args;
    va_start (args, format);
    vsprintf (buffer,format, args);
    va_end (args);
    BOOST_LOG_SEV(lg, debug) <<buffer;

}

void LogInfo_(const char *format,...)
{
    char buffer[256];
    va_list args;
    va_start (args, format);
    vsprintf (buffer,format, args);
    //   perror (buffer);
    va_end (args);
    BOOST_LOG_SEV(lg, info) << buffer;

}

void LogErr_(const char *format,...)
{
    char buffer[256];
    va_list args;
    va_start (args, format);
    vsprintf (buffer,format, args);
  //  perror (buffer);
    va_end (args);
    BOOST_LOG_SEV(lg, error) << buffer;

}

void LogWrn_(const char *format,...)
{
    char buffer[256];
    va_list args;
    va_start (args, format);
    vsprintf (buffer,format, args);
   // perror (buffer);
    va_end (args);
    BOOST_LOG_SEV(lg, warning) << buffer;

}



