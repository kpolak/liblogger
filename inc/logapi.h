#ifndef LOGAPI_H
#define LOGAPI_H

/*  The MIT License (MIT)
Copyright (c) 2016 <Konrad Polak (kpolak@kpolak.eu)>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE. */

#ifdef LOG_ON
    #define initLog() initLog_()
    #define LogInfo(...) LogInfo_( __VA_ARGS__)
    #define LogDebug(...) LogDebug_(__VA_ARGS__)
    #define LogErr(...) LogErr_(__VA_ARGS__)
    #define LogWrn(...) LogWrn_(__VA_ARGS__)
#else
    #define initLog() do{}while (false)
    #define LogInfo(...) do{}while (false)
    #define LogDebug(...) do{}while (false)
    #define LogErr(...) do{}while (false)
    #define LogWrn(...) do{}while (false)
#endif


void initLog_();

void LogDebug_(const char *format,...);
void LogInfo_(const char *format,...);
void LogWrn_(const char *format,...);
void LogErr_(const char *format,...);


#endif
